package com.wwj231.ngohelp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NgoHelpApplication {

    public static void main(String[] args) {
        SpringApplication.run(NgoHelpApplication.class, args);
    }

}
