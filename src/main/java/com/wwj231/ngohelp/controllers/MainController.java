package com.wwj231.ngohelp.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Controller for displaying welcoming page
 */

@Controller
@RequestMapping({"/welcome", "/home", "/index"})
public class MainController {
    private static final Logger logger = LoggerFactory.getLogger(MainController.class);

    @GetMapping
    public String getMainPage(Model model) {
        logger.info("Main Controller.getMainPage -> index");
        return "index";
    }
}
